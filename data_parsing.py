from string import hexdigits
import cv2
import xml.etree.ElementTree as ET

import random
import os

path = "./dataset/txt_annotations"
try:
    os.mkdir(path)
except FileExistsError:
    print('Folder vec postoj')    

# print(folder)
# print(filename)
# print(width)
# print(height)

for j in range(852):
    tree = ET.parse("./dataset/annotations/maksssksksss" + str(j) + ".xml")
    root = tree.getroot()

    try:
        file = open(path + "/maksssksksss" + str(j) + ".txt", "w")
    except:
        print("Nema fajla")
        break

    # folder = root[0].text
    # filename = root[1].text

    # print(folder)
    # print(filename)

    width = int(root[2][0].text)
    height = int(root[2][1].text)

    # print(width)
    # print(height)


    # img = cv2.imread("./dataset/images/" + filename)

    lines = []
    for i in range(4, len(root)):
        # print(root[i].tag)
        label = root[i][0].text
        x1 = int(root[i][5][0].text)  
        y1 = int(root[i][5][1].text)
        x2 = int(root[i][5][2].text)
        y2 = int(root[i][5][3].text)


        # druga verzija
        # x = (x1+x2)/2
        # y = (y1+y2)/2

        x = (x1+x2)/2-1
        y = (y1+y2)/2-1

        w = (x2 - x1)/width
        h = (y2 - y1)/height 
        x = x/width 
        y = y/height

        # print(str(x), str(y), str(w), str(h))
        # print(x1, y1, x2, y2)
        # if label == 'without_mask':
            # cv2.rectangle(img, (x1,y1), (x2,y2), (0,0,255), thickness=5)
        # else:
            # cv2.rectangle(img, (x1,y1), (x2,y2), (0,255,0), thickness=5)
        # print(x1, y1, x2, y2)

        ret = ""
        if label == 'without_mask':
            ret += "0 "   
        elif label == 'with_mask' :
            ret += "1 "
        else:
            ret += "2 "
        ret +=str(x) + " " + str(y) + " " + str(w) + " " + str(h)  + "\n"
        # print(ret)
        lines.append(ret)
        
    file.writelines(lines)
    file.close()

    # cv2.rectangle(img, (1,1), (50,70), (255,0,0))

    # cv2.imshow('Image', img)
    # cv2.waitKey(0)