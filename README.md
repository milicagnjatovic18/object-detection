# Object detection

Projekat za predmet računarska inteligencija.
Korišćen YOLO algoritam.

---
## Biblioteke
* OpenCV
---
## Pokretanje gotove mreže
* Svi potrebni fajlovi su u folderu darknet_data 
* Preuzeti darknet sa https://github.com/artynet/darknet-alexeyAB
* Uraditi build za darknet (dosta komplikovano za Windows, pogledati uputstvo)
* U darknetu napraviti folder data i u njega prekopirati obj.data
* U cfg folder prekopirati yolov3-custom.cfg
* Napraviti folder weights za težine i u njega dodati yolov3-custom_final.weights
* Pokrenuti darknet sa komandom:
```
./darknet.exe detector test data/obj.data cfg/yolov3-custom.cfg  weights/ yolov3-custom_final.weights {putanja do slike koje zelite da testirate}
```
* Ili, ukoliko želite da testirate video, sa komandom:
```
./darkent.exe detector demo data/obj.data cfg/yolov3-custom.cfg weights/yolov3-custom_final.weights -ext_output {putanja do videa koji zelite da testirate} -out_filename {ime fajla za izlaz}
```